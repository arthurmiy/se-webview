//Import scripts
import * as THREE from "./three.module.js";
import { FBXLoader } from "./loaders/FBXLoader.js";
import { GLTFLoader } from "./loaders/GLTFLoader.js";
import { DRACOLoader } from "./loaders/DRACOLoader.js";
import { NexusObject } from "./nexus_three.js";

/*
    ----------------------------------------------CONFIGURATION PARAMETERS
*/

//todo interaction
//Camera parameters
var cameraFOV;
var cameraNear;
var cameraFar;
//model parameters
var modelPath;
var loaderFBX = new FBXLoader();
var loaderGltf = new GLTFLoader();
var dracoLoader;
var keyAnimations = new Object();
let clock;

//3D elements
var camera, scene, renderer, light; //basic render objects
//html elements
var container;
//animation elements
var tween; //camera
var tween2; //3d objs

var currentSlide = 0;

/*
    -----------------------------------------------Function Calls
*/
/*
          3D Environment
      */
init();
animate();

/*
    ---------------------------------------------------Initialization functions
*/
function initParameters() {
  //Camera
  cameraFOV = window.cameraFOV ?? 45;
  cameraNear = window.cameraNear ?? 10;
  cameraFar = window.cameraFar ?? 90000;
}

// Initialize 3D environment
function init() {
  initParameters();
  //initialize document container
  container = document.createElement("div");
  document.body.appendChild(container);

  //init clock
  clock = new THREE.Clock();

  //init camera
  camera = new THREE.PerspectiveCamera(
    cameraFOV,
    window.innerWidth / window.innerHeight,
    cameraNear,
    cameraFar
  );

  //create the scene
  scene = new THREE.Scene();

  //create the light
  light = new THREE.HemisphereLight(0xffffff, 0x444444);
  light.position.set(0, 500, 200);
  scene.add(light);

  //create renderer
  renderer = new THREE.WebGLRenderer({ antialias: false });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  container.appendChild(renderer.domElement);

  window.addEventListener("resize", onWindowResize, false);

  loadModelsFromList(window.objectsToLoad, "scene");
}

function render() {
  Nexus.beginFrame(renderer.getContext());
  renderer.render(scene, camera);
  Nexus.endFrame(renderer.getContext());
}

/*
    ------------------------------------------Window events
*/
function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

/*
    -----------------------------------------------Camera movement
*/
function move(to) {
  let duration = 100;

  let from = getCamCurrentPosition();
  tween = new TWEEN.Tween(from)
    .to(to, duration)
    .easing(TWEEN.Easing.Linear.None) // TWEEN.Easing.Quadratic.InOut ...
    .onUpdate(function () {
      camera.position.set(from.x, from.y, from.z);
      camera.rotation.set(from.rx, from.ry, from.rz);
      //controls.target.set(from.rx, from.ry, from.rz);
    })
    .start();
}

function scrollStatus() {
  let doc = document.getElementById("content");
  let part = Math.floor(doc.scrollTop / doc.clientHeight);
  let tolerance = 30;
  let percentage =
    (100 * (doc.scrollTop - part * doc.clientHeight)) / doc.clientHeight;
  if (percentage < tolerance) {
    percentage = 0;
  } else {
    percentage = (100 * (percentage - tolerance)) / (100 - tolerance);
  }
  return {
    index: part,
    percent: percentage,
  };
}

function scrollIt() {
  let scrollInfos = scrollStatus();
  if (currentSlide != scrollInfos.index) {
    slideChangeCallback(currentSlide, scrollInfos.index);
    currentSlide = scrollInfos.index;
  } else {
    //slide update
    slidePositionUpdateCallback(scrollInfos);
  }

  if (scrollInfos.index + 1 < positionsList.length) {
    var result = percentage2position(
      scrollInfos.percent,
      positionsList[scrollInfos.index],
      positionsList[scrollInfos.index + 1]
    );
    move(result);
  }
}

function percentage2position(percentage, from, to) {
  var final = {
    x: 0,
    y: 0,
    z: 0,
    rx: 0,
    ry: 0,
    rz: 0,
  };

  final.x = from.x + (percentage / 100) * (to.x - from.x);
  final.y = from.y + (percentage / 100) * (to.y - from.y);
  final.z = from.z + (percentage / 100) * (to.z - from.z);
  final.rx = from.rx + (percentage / 100) * (to.rx - from.rx);
  final.ry = from.ry + (percentage / 100) * (to.ry - from.ry);
  final.rz = from.rz + (percentage / 100) * (to.rz - from.rz);

  return final;
}

function stop() {
  tween.stop();
}

//render loop
function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
  //controls.update();
  const mixerUpdateDelta = clock.getDelta();
  for (var m in keyAnimations) {
    keyAnimations[m].mixer.update(mixerUpdateDelta);
  }

  scrollIt();
  renderer.render(scene, camera);
}

function getCamCurrentPosition() {
  return {
    x: camera.position.x,
    y: camera.position.y,
    z: camera.position.z,
    rx: camera.rotation.x,
    ry: camera.rotation.y,
    rz: camera.rotation.z,
  };
}

/*
    ---------------------------------------------------------Load functions
*/

function getLoadFunction(path) {
  if (path.endsWith("fbx")) {
    return addFbxToScene;
  } else if (path.endsWith("nxs")) {
    //todo Nexus
    return addNexusToScene;
  } else if (path.endsWith("glb")) {
    //gltf
    return addGlbToScene;
  } else if (path.endsWith("text")) {
    return addTextToScene;
  }
}

function loadModelsFromList(list, placeToAdd) {
  //todo add to group in cases where there is a model to load and children
  list.forEach(function (item) {
    let tmpGroup;
    if (item.children != null) {
      //list of models
      tmpGroup = new THREE.Group();
      tmpGroup.name = item.name;
      scene.add(tmpGroup);
      loadModelsFromList(item.children, item.name);
    }
    if (item.path != null) {
      //load model and apply transformations
      if (tmpGroup != null) {
        //existe grupo e tem modelo pra carregar
        //todo
      } else {
        //só tem modelo pra adicionar
        let loadFunction = getLoadFunction(item.path);
        loadFunction(item, placeToAdd);
      }
    } else {
      //path null -> verify children
    }
  });
}

function addFbxToScene(item, placeToAdd) {
  loaderFBX.load(item.path, function (object) {
    object.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true;
        child.receiveShadow = true;
      }
    });

    if (placeToAdd == "scene") {
      scene.add(applyDefinedTransformations(item, object));
    } else {
      scene
        .getObjectByName(placeToAdd)
        .add(applyDefinedTransformations(item, object));
    }

    if (item.onLoadAnimation != null) {
      animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
    }
  });
}

function addNexusToScene(item) {
  var object = new NexusObject(item.path, renderer, render);
  scene.add(object);
}

function addGlbToScene(item, placeToAdd) {
  if (dracoLoader == null) {
    dracoLoader = new DRACOLoader();
    dracoLoader.setDecoderPath("./java/loaders/gltf/");
    loaderGltf.setDRACOLoader(dracoLoader);
  }

  loaderGltf.load(
    item.path,
    function (gltf) {
      let object = gltf.scene;

      // object.traverse(function (child) {
      //   if (child.isMesh) {
      //     child.castShadow = true;
      //     child.receiveShadow = true;
      //   }
      // });

      //if animation is present saves data
      /*
        format
        model_name = {
          mixer: Three.Mixer,
          animation1: Mixer.Action
        }
      */
      if (gltf.animations.length > 0) {
        keyAnimations[item.name] = {
          mixer: new THREE.AnimationMixer(object),
        };
        let i;
        for (i = 0; i < gltf.animations.length; i++) {
          keyAnimations[item.name][gltf.animations[i].name] = keyAnimations[
            item.name
          ].mixer.clipAction(gltf.animations[i]);
        }
      }

      if (placeToAdd == "scene") {
        scene.add(applyDefinedTransformations(item, object));
      } else {
        scene
          .getObjectByName(placeToAdd)
          .add(applyDefinedTransformations(item, object));
      }

      if (item.onLoadAnimation != null) {
        animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
      }
    },
    function (xhr) {
      console.log((xhr.loaded / xhr.total) * 100 + "% loaded");
    },
    // called when loading has errors
    function (error) {
      console.log("An error happened");
    }
  );
}

function CreateAndAddText(
  font,
  size,
  height,
  curveSegments,
  bevelThickness,
  bevelSize,
  bevelEnabled,
  materials,
  item,
  hover,
  placeToAdd
) {
  let extracted = item.path.substring(0, item.path.length - ".text".length);
  let textGeo = new THREE.TextGeometry(extracted, {
    font,

    size: size,
    height: height,
    curveSegments: curveSegments,

    bevelThickness: bevelThickness,
    bevelSize: bevelSize,
    bevelEnabled: bevelEnabled,
  });

  textGeo.computeBoundingBox();
  textGeo.computeVertexNormals();

  const triangle = new THREE.Triangle();

  // "fix" side normals by removing z-component of normals for side faces
  // (this doesn't work well for beveled geometry as then we lose nice curvature around z-axis)

  if (!bevelEnabled) {
    const triangleAreaHeuristics = 0.1 * (height * size);

    for (let i = 0; i < textGeo.faces.length; i++) {
      const face = textGeo.faces[i];

      if (face.materialIndex == 1) {
        for (let j = 0; j < face.vertexNormals.length; j++) {
          face.vertexNormals[j].z = 0;
          face.vertexNormals[j].normalize();
        }

        const va = textGeo.vertices[face.a];
        const vb = textGeo.vertices[face.b];
        const vc = textGeo.vertices[face.c];

        const s = triangle.set(va, vb, vc).getArea();

        if (s > triangleAreaHeuristics) {
          for (let j = 0; j < face.vertexNormals.length; j++) {
            face.vertexNormals[j].copy(face.normal);
          }
        }
      }
    }
  }

  const centerOffset =
    -0.5 * (textGeo.boundingBox.max.x - textGeo.boundingBox.min.x);

  textGeo = new THREE.BufferGeometry().fromGeometry(textGeo);

  let object = new THREE.Group();

  let textMesh1 = new THREE.Mesh(textGeo, materials);

  textMesh1.position.x = centerOffset;
  textMesh1.position.y = hover;
  textMesh1.position.z = 0;

  object.add(textMesh1);

  if (item.tProperties.mirror) {
    let textMesh2 = new THREE.Mesh(textGeo, [
      materials[0].clone(),
      materials[1].clone(),
    ]);

    textMesh2.position.x = centerOffset;
    textMesh2.position.y = -hover;
    textMesh2.position.z = height;
    textMesh2.rotation.x = Math.PI;
    textMesh2.rotation.y = Math.PI * 2;
    textMesh2.maxOpacity = item.tProperties.mirrorOpacity;
    setOpacity(textMesh2, item.tProperties.mirrorOpacity);
    object.add(textMesh2);
  }

  return applyDefinedTransformations(item, object);

  if (item.onLoadAnimation != null) {
    animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
  }
}

function applyDefinedTransformations(item, object) {
  if (item.rotation != null) {
    object.rotation.set(item.rotation[0], item.rotation[1], item.rotation[2]);
  }
  if (item.scale != null) {
    object.scale.set(item.scale[0], item.scale[1], item.scale[2]);
  }
  if (item.position != null) {
    if (item.position.length > 1) {
      //todo insert copies
      var tempGroup = new THREE.Group();
      item.position.forEach((pos) => {
        object.position.set(pos.x, pos.y, pos.z);
        var tmpMesh = object.clone();
        tempGroup.add(tmpMesh);
      });
      tempGroup.name = item.name;
      return tempGroup;
    } else {
      object.name = item.name;
      object.position.set(
        item.position[0].x,
        item.position[0].y,
        item.position[0].z
      );
      return object;
    }
  } else {
    object.name = item.name;
    return object;
  }
}
function addTextToScene(item, placeToAdd) {
  //default values
  let fontName = item.tProperties.font ?? "helvetiker";
  let fontWeight = item.tProperties.fontWeight ?? "bold";
  let size = item.tProperties.size ?? 70;
  let height = item.tProperties.height ?? 20;
  let curveSegments = item.tProperties.curveSegments ?? 4;
  let bevelThickness = item.tProperties.bevelThickness ?? 2;
  let bevelSize = item.tProperties.bevelSize ?? 1.5;
  let bevelEnabled = item.tProperties.bevelEnabled ?? true;
  let color = item.tProperties.color ?? 0xffffff;
  let hover = item.tProperties.hover ?? 20;

  //load font
  const Floader = new THREE.FontLoader();
  Floader.load(
    "./fonts/" + fontName + "_" + fontWeight + ".typeface.json",
    function (response) {
      let font = response;

      let materials = [
        new THREE.MeshPhongMaterial({
          color: new THREE.Color(color),
          flatShading: true,
        }), // front
        new THREE.MeshPhongMaterial({ color: new THREE.Color(color) }), // side
      ];

      if (placeToAdd == "scene") {
        scene.add(
          CreateAndAddText(
            font,
            size,
            height,
            curveSegments,
            bevelThickness,
            bevelSize,
            bevelEnabled,
            materials,
            item,
            hover
          )
        );
      } else {
        scene
          .getObjectByName(placeToAdd)
          .add(
            CreateAndAddText(
              font,
              size,
              height,
              curveSegments,
              bevelThickness,
              bevelSize,
              bevelEnabled,
              materials,
              item,
              hover
            )
          );
      }

      if (item.onLoadAnimation != null) {
        animateKeys([item.onLoadAnimation], item.onLoadAnimation.duration);
      }
    }
  );
}

/*
    ---------------------------------------------------------Animation control
*/
function getAnimateKeysInitialState(keys) {
  let initialState = [];

  keys.forEach(function (value) {
    value.poseKeys.forEach(function (pose) {
      //todo only for array props
      if (
        pose.property == "rotation" ||
        pose.property == "position" ||
        pose.property == "scale"
      ) {
        if (pose.target == "children") {
          scene
            .getObjectByName(value.objName)
            .children.forEach(function (child) {
              if (pose.initialState == null) {
                initialState.push(child[pose.property].toArray());
              } else {
                initialState.push(pose.initialState);
              }
            });
        } else {
          if (pose.initialState == null) {
            initialState.push(
              scene.getObjectByName(value.objName)[pose.property].toArray()
            );
          } else {
            initialState.push(pose.initialState);
          }
        }
      } else if (pose.property == "Opacity") {
        if (pose.initialState == null) {
          initialState.push(getOpacity(scene.getObjectByName(value.objName)));
        } else {
          initialState.push(pose.initialState);
        }
      } else if (pose.property == "EmissiveIntensity") {
        if (pose.initialState == null) {
          initialState.push(
            getEmissiveIntensity(scene.getObjectByName(value.objName))
          );
        } else {
          initialState.push(pose.initialState);
        }
      }
    });
  });
  return initialState;
}

function calculateValueFromPercentage(initial, final, percentage, type) {
  if (type == "absolute") {
    return initial + (final - initial) * percentage;
  } else if (type == "relative") {
    return initial + final * percentage;
  }
}

function animateKeys(keys, time) {
  let duration = time ?? 700;
  let initialState = getAnimateKeysInitialState(keys);
  var valueFrom = { perc: 0 };
  var valueTo = { perc: 1 };

  tween2 = new TWEEN.Tween(valueFrom)
    .to(valueTo, duration)
    .easing(TWEEN.Easing.Quadratic.InOut) // TWEEN.Easing.Quadratic.InOut ...
    .onUpdate(function () {
      var counter = 0;
      //search all keys
      keys.forEach(function (value) {
        value.poseKeys.forEach(function (pose) {
          if (
            pose.property == "rotation" ||
            pose.property == "position" ||
            pose.property == "scale"
          ) {
            if (pose.target == "children") {
              scene
                .getObjectByName(value.objName)
                .children.forEach(function (child, index) {
                  scene
                    .getObjectByName(value.objName)
                    .children[index][pose.property].set(
                      calculateValueFromPercentage(
                        initialState[counter][0],
                        pose.value[0],
                        valueFrom.perc,
                        pose.type
                      ),
                      calculateValueFromPercentage(
                        initialState[counter][1],
                        pose.value[1],
                        valueFrom.perc,
                        pose.type
                      ),
                      calculateValueFromPercentage(
                        initialState[counter][2],
                        pose.value[2],
                        valueFrom.perc,
                        pose.type
                      )
                    );
                  counter++;
                });
            } else {
              scene
                .getObjectByName(value.objName)
                [pose.property].set(
                  calculateValueFromPercentage(
                    initialState[counter][0],
                    pose.value[0],
                    valueFrom.perc,
                    pose.type
                  ),
                  calculateValueFromPercentage(
                    initialState[counter][1],
                    pose.value[1],
                    valueFrom.perc,
                    pose.type
                  ),
                  calculateValueFromPercentage(
                    initialState[counter][2],
                    pose.value[2],
                    valueFrom.perc,
                    pose.type
                  )
                );
              counter++;
            }
          } else {
            if (pose.property.toUpperCase() == "Opacity".toUpperCase()) {
              setOpacity(
                scene.getObjectByName(value.objName),
                calculateValueFromPercentage(
                  initialState[counter],
                  pose.value,
                  valueFrom.perc,
                  pose.type
                )
              );
            } else if (
              pose.property.toUpperCase() == "EmissiveIntensity".toUpperCase()
            ) {
              setEmissive(
                scene.getObjectByName(value.objName),
                pose.color,
                calculateValueFromPercentage(
                  initialState[counter],
                  pose.value,
                  valueFrom.perc,
                  pose.type
                )
              );
            }

            counter++;
          }
        });
      });
    })
    .start();
}

/*
          --------------------------------------------------property change
      */
function setOpacity(obj, opacityIn) {
  let opacity;
  if (obj.maxOpacity != null) {
    if (obj.maxOpacity < opacityIn) {
      opacity = obj.maxOpacity;
    } else {
      opacity = opacityIn;
    }
  } else {
    opacity = opacityIn;
  }
  obj.children.forEach((child) => {
    setOpacity(child, opacity);
  });
  if (obj.material) {
    obj.material.opacity = opacity;
    obj.material.transparent = true;
    if (obj.material.length > 0) {
      obj.material.forEach((child) => {
        child.opacity = opacity;
        child.transparent = true;
      });
      let test = obj.material;
    }
  }
}

function setEmissive(obj, emissiveColor, emissiveIntensity) {
  obj.children.forEach((child) => {
    setEmissive(child, emissiveColor, emissiveIntensity);
  });
  if (obj.material) {
    obj.material.emissive = new THREE.Color(emissiveColor ?? 0xff0000);

    if (emissiveIntensity != null) {
      obj.material.emissiveIntensity = emissiveIntensity;
    }
  }
}

function getEmissiveIntensity(obj) {
  let tmp, intensity;
  intensity = 1;
  obj.children.forEach((child) => {
    tmp = getEmissiveIntensity(child);
    if (tmp != null) {
      if (tmp < intensity && tmp >= 0) {
        intensity = tmp;
      }
    }
  });
  if (obj.material) {
    return obj.material.emissiveIntensity;
  }
  return intensity;
}

function getOpacity(obj) {
  let tmp, opac;
  opac = 1;
  obj.children.forEach((child) => {
    tmp = getOpacity(child);
    if (tmp != null) {
      if (tmp < opac && tmp >= 0) {
        opac = tmp;
      }
    }
  });
  if (obj.material) {
    return obj.material.opacity;
  }
  return opac;
}

///////////////////interaction

function onDocumentMouseUp(event) {
  //event.preventDefault();

  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

  var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
  var pos = camera.position;
  var ray = new THREE.Raycaster(
    pos,
    vector.unproject(camera).sub(camera.position).normalize()
  );

  var intersects = ray.intersectObjects(objOne, true);

  if (intersects.length > 0) {
    objClickedCallback(intersects[0].object);
    //window.open("./free.html", "_self");
  }
}
function getValidParentName(object) {
  let tmp = object.parent.name ?? "-";
  if (tmp == "") {
    return getValidParentName(object.parent);
  } else {
    return tmp;
  }
}
function verifyParentName(object, name) {
  let tmp = object.parent;
  if (tmp == null) {
    return false;
  } else {
    if (tmp.name == name) {
      return true;
    } else {
      return verifyParentName(tmp, name);
    }
  }
}

function onDocumentMouseMove(event) {
  //event.preventDefault();

  if (objOne.length != clickableObjs) {
    objOne = clickableObjs.map((obj) => scene.getObjectByName(obj));
  }

  mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
  mouse.y = -(event.clientY / window.innerHeight) * 2 + 1;

  var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
  var pos = camera.position;
  var ray = new THREE.Raycaster(
    pos,
    vector.unproject(camera).sub(camera.position).normalize()
  );

  var intersects = ray.intersectObjects(objOne, true);

  let currentName;

  if (intersects.length > 0) {
    //touched
    currentName = intersects[0].object.name;

    if (currentName == "") {
      currentName = getValidParentName(intersects[0].object);
    }

    if (over == false || lastOver != currentName) {
      over = true;
      lastOver = currentName;
      objOverCallBack(intersects[0].object);
      lastOverObj = intersects[0].object;
    }
  } else {
    if (over == true) {
      over = false;
      objOverOutCallback(lastOverObj);
      lastOver = "";
      lastOverObj = null;
    }
  }
}

var mouse = new THREE.Vector2(); // create once
var currentBtnFocus = "btnCloseImg";
var objOne = [];
var lastOver = "";
var lastOverObj;
var over;

document.addEventListener("mousedown", onDocumentMouseUp, false);
document.addEventListener("mousemove", onDocumentMouseMove, false);
document.getElementById("btn1").onmouseover = onBtn1Over;
document.getElementById("btn1").onmouseout = onBtn1MouseOut;
document.getElementById("btn1").onclick = onBtn1Click;

function btnElementToGreen(element) {
  document.getElementById(element).classList.remove("toTransparent");
  document.getElementById(element).classList.add("toGreen");
}

function btnElementToTranp(element) {
  document.getElementById(element).classList.remove("toGreen");
  document.getElementById(element).classList.add("toTransparent");
}

function onBtn1Over() {
  btnElementToGreen(currentBtnFocus);
  animateKeys([
    window.animationKeys.sec1.highlightOn,
    window.animationKeys.sec2.highlightOn,
  ]);
  //keyAnimations.se.jump.play();
}

function onBtn1MouseOut() {
  btnElementToTranp(currentBtnFocus);
  animateKeys([
    window.animationKeys.sec1.highlightOff,
    window.animationKeys.sec2.highlightOff,
  ]);
  //keyAnimations.se.jump.stop();
}

function onBtn1Click() {
  btnElementToTranp(currentBtnFocus);

  if (currentBtnFocus == "btnOpenImg") {
    currentBtnFocus = "btnCloseImg";
    document.getElementById("btn1").innerHTML = "Abrir chave seccionadora";
    animateKeys([
      window.animationKeys.sec1.close,
      window.animationKeys.sec2.close,
    ]);
  } else {
    currentBtnFocus = "btnOpenImg";
    document.getElementById("btn1").innerHTML = "Fechar chave seccionadora";
    animateKeys([
      window.animationKeys.sec1.open,
      window.animationKeys.sec2.open,
    ]);
  }
  btnElementToGreen(currentBtnFocus);
}

function slideChangeCallback(oldSlide, newSlide) {
  //callback to slide change
  if (newSlide == 1) {
    animateKeys([window.animationKeys.se.xray]);
  }
  if (oldSlide == 1) {
    animateKeys([window.animationKeys.se.increaseOpacity]);
  }

  if (newSlide == 4) {
    animateKeys([window.animationKeys.sinoticos.highlightOn]);
  }
  if (oldSlide == 4) {
    animateKeys([window.animationKeys.sinoticos.highlightOff]);
  }

  if (newSlide == 5) {
    animateKeys([window.animationKeys.setor1.highlightOn]);
  }
  if (oldSlide == 5) {
    animateKeys([window.animationKeys.setor1.highlightOff]);
  }

  if (newSlide == 6) {
    animateKeys([window.animationKeys.setor2.highlightOn]);
  }
  if (oldSlide == 6) {
    animateKeys([window.animationKeys.setor2.highlightOff]);
  }
}
function slidePositionUpdateCallback(scrollInfos) {
  //callback to slide change
  if (scrollInfos.percent < 10 && scrollInfos.index == 0) {
    animateKeys([window.animationKeys.se.showUp], 2000);
    animateKeys([window.animationKeys.ortophoto.fromInvisible], 10000);
  }
  if (scrollInfos.index == 1 && getOpacity(scene.getObjectByName("se")) == 1) {
    animateKeys([window.animationKeys.se.xray]);
  }
}

function objClickedCallback(obj) {
  // console.log("touched:" + objName);
  if (verifyParentName(obj, "towers")) {
    animateKeys([window.animationKeys.towers.showDown]);
  } else if (verifyParentName(obj, "abrigo") || obj.name == "abrigo") {
    animateKeys([window.animationKeys.towers.showUp]);
  }
}

function objOverCallBack(obj) {
  // console.log("over:" + objName);
  if (verifyParentName(obj, "chaves")) {
    animateKeys([window.animationKeys.aviso.in]);
  }
}

function objOverOutCallback(obj) {
  // console.log("out from " + objName);
  if (verifyParentName(obj, "chaves")) {
    animateKeys([window.animationKeys.aviso.out]);
  }
}
