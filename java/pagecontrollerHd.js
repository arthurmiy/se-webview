//Import scripts
import * as THREE from "./three.module.js";
import { FBXLoader } from "./loaders/FBXLoader.js";
import { GLTFLoader } from "./loaders/GLTFLoader.js";
import { NexusObject } from "./nexus_three.js";

/*
    CONFIGURATION PARAMETERS
*/
//Camera parameters
var cameraFOV;
var cameraNear;
var cameraFar;
//model parameters
var modelPath;

function initParameters() {
  //Camera
  cameraFOV = window.cameraFOV ?? 45;
  cameraNear = window.cameraNear ?? 10;
  cameraFar = window.cameraFar ?? 90000;
  modelPath = window.modelPath ?? "./models/worst.fbx";
}

var container;
var camera, scene, renderer, light; //basic render objects
var tween;
var tween2;
var tween3;
var raycaster = new THREE.Raycaster(); // create once
var mouse = new THREE.Vector2(); // create once
var objOne;
var over = false;
var chaves;
var switchOpen = false;
var switchZero;
var switchPi;
var angleTo;
var angleFrom;
var seModel;

// document.addEventListener('mousedown', onDocumentMouseUp, false);
// document.addEventListener('mousemove', onDocumentMouseMove, false);
//document.getElementById("btn1").onclick = toggleSwitch;
document.getElementById("btn1").onmouseover = onBtn1Over;
document.getElementById("btn1").onmouseout = onBtn1MouseOut;

/*
          INTERATIVIDADE
      */
function setOpacity(obj, opacity) {
  obj.children.forEach((child) => {
    setOpacity(child, opacity);
  });
  if (obj.material) {
    obj.material.opacity = opacity;
    obj.material.transparent = true;
  }
}

// Abre e fecha chave seccionadora
window.toggleSwitch = function toggleSwitch() {
  switchOpen = !switchOpen;

  if (switchOpen) {
    document.getElementById("btnOpenImg").classList.remove("toTransparent");
    document.getElementById("btnOpenImg").classList.add("toGreen");
    document.getElementById("btnCloseImg").classList.remove("toGreen");
    document.getElementById("btnCloseImg").classList.add("toTransparent");
  } else {
    document.getElementById("btnCloseImg").classList.remove("toTransparent");
    document.getElementById("btnCloseImg").classList.add("toGreen");
    document.getElementById("btnOpenImg").classList.remove("toGreen");
    document.getElementById("btnOpenImg").classList.add("toTransparent");
  }

  if (switchOpen) {
    document.getElementById("btn1").innerHTML = "Fechar chave seccionadora";
  } else {
    document.getElementById("btn1").innerHTML = "Abrir chave seccionadora";
  }
  toggle3DSwitch();
};

function onBtn1Over() {
  if (switchOpen) {
    document.getElementById("btnOpenImg").classList.remove("toTransparent");
    document.getElementById("btnOpenImg").classList.add("toGreen");
  } else {
    document.getElementById("btnCloseImg").classList.remove("toTransparent");
    document.getElementById("btnCloseImg").classList.add("toGreen");
  }
  setOpacity(seModel, 0.3);
}

function onBtn1MouseOut() {
  if (switchOpen) {
    document.getElementById("btnOpenImg").classList.remove("toGreen");
    document.getElementById("btnOpenImg").classList.add("toTransparent");
  } else {
    document.getElementById("btnCloseImg").classList.remove("toGreen");
    document.getElementById("btnCloseImg").classList.add("toTransparent");
  }
  setOpacity(seModel, 1);
}

// Link via Objeto 3D
// function onDocumentMouseUp(event) {
//     event.preventDefault();

//     mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
//     mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

//     var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
//     var pos = camera.position;
//     var ray = new THREE.Raycaster(pos, vector.unproject(camera).sub(camera.position).normalize());

//     var intersects = ray.intersectObjects([objOne],true);

//     if (intersects.length > 0) {
//       console.log("touched:" + intersects[0]);
//       window.open("./free.html", "_self")
//     }
//     else {
//       console.log("not touched");
//     }
//   }

//   //Permite identificar se o mouse está sobrepondo elemento clicável
//   function onDocumentMouseMove(event) {
//       event.preventDefault();

//       mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
//       mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;

//       var vector = new THREE.Vector3(mouse.x, mouse.y, 0.5);
//       var pos = camera.position;
//       var ray = new THREE.Raycaster(pos, vector.unproject(camera).sub(camera.position).normalize());

//       var intersects = ray.intersectObjects([objOne], true);

//       if (intersects.length > 0) {
//         //touched
//         over = true;
//       }
//       else {
//         over=false;
//       }

//        if (objOne != null) {
//       if (over) {
//         // objOne.children[0].currentHex = objOne.children[0].material.opacity=0.5;
//         var INTERSECTED = objOne.children[0];
//         INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
//         INTERSECTED.material.emissive.setHex(0x990000);
//         document.getElementById("content").style.cursor = "pointer";
//       } else {
//         var INTERSECTED = objOne.children[0];
//         INTERSECTED.currentHex = INTERSECTED.material.emissive.getHex();
//         INTERSECTED.material.emissive.setHex(0x000000);
//         document.getElementById("content").style.cursor = "auto";
//       }
//     }
//     }

//fading effect
document.getElementById("content").onscroll = () => {
  let scrollInfos = scrollStatus();
  let tmp = document.getElementsByClassName("fullscrn");

  if (scrollInfos.index + 1 < tmp.length) {
    tmp[scrollInfos.index + 1].children[0].style.opacity =
      scrollInfos.percent / 100;
    tmp[scrollInfos.index].children[0].style.opacity =
      (100 - scrollInfos.percent) / 100;
  }
};

document;

/*
          3D Environment
      */
init();
animate();

// Initialize 3D environment
function init() {
  initParameters();
  //initialize document container
  container = document.createElement("div");
  document.body.appendChild(container);

  //init camera
  camera = new THREE.PerspectiveCamera(
    cameraFOV,
    window.innerWidth / window.innerHeight,
    cameraNear,
    cameraFar
  );

  //create the scene
  scene = new THREE.Scene();

  //create the light
  light = new THREE.HemisphereLight(0xffffff, 0x444444);
  light.position.set(0, 500, 200);
  scene.add(light);

  //create renderer
  renderer = new THREE.WebGLRenderer({ antialias: false });
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.shadowMap.enabled = true;
  container.appendChild(renderer.domElement);

  window.addEventListener("resize", onWindowResize, false);

  // model
  var loader = new FBXLoader();
  loader.load(modelPath, function (object) {
    object.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true;
        child.receiveShadow = true;
      }
    });
    seModel = object;
    scene.add(seModel);
  });

  // model
  var loader = new FBXLoader();
  loader.load("./models/solo.fbx", function (object) {
    object.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true;
        child.receiveShadow = true;
      }
    });
    scene.add(object);
  });

  //chave seccionadora: carrega elementos de animação
  var loader = new FBXLoader();

  var scaleChave = 1;
  loader.load("./models/sec1.fbx", function (object) {
    var tmpMesh;
    object.traverse(function (child) {
      if (child.isMesh) {
        child.castShadow = true;
        child.receiveShadow = true;

        tmpMesh = child.clone();
      }
    });
    if (chaves == null) {
      chaves = new THREE.Group();
    }

    //POSIÇÃO 1.1
    var p = [-4378.6, 303, -1271];
    tmpMesh.position.set(p[0], p[1], p[2]);
    chaves.add(tmpMesh.clone());

    //POSIÇÃO 1.2
    p[2] = p[2] + 290 / scaleChave;
    tmpMesh.position.set(p[0], p[1], p[2] + 14);
    chaves.add(tmpMesh.clone());

    //POSIÇÃO 1.3
    p[2] = p[2] + 260 / scaleChave;
    tmpMesh.position.set(p[0] - 4, p[1], p[2] + 26);
    chaves.add(tmpMesh.clone());

    var loader = new FBXLoader();

    loader.load("./models/sec2.fbx", function (object) {
      var tmpMesh;
      object.traverse(function (child) {
        if (child.isMesh) {
          child.castShadow = true;
          child.receiveShadow = true;

          tmpMesh = child.clone();
        }
      });
      if (chaves == null) {
        chaves = new THREE.Group();
      }

      //POSIÇÃO 1.1
      var p = [-4178.1, 303, -1271];
      tmpMesh.position.set(p[0], p[1], p[2]);
      chaves.add(tmpMesh.clone());

      //POSIÇÃO 1.2
      p[2] = p[2] + 290 / scaleChave;
      tmpMesh.position.set(p[0], p[1], p[2] + 14);
      chaves.add(tmpMesh.clone());

      //POSIÇÃO 1.3
      p[2] = p[2] + 260 / scaleChave;
      tmpMesh.position.set(p[0] - 4, p[1], p[2] + 26);
      chaves.add(tmpMesh.clone());

      chaves.scale.set(scaleChave, scaleChave, scaleChave);

      // chaves.add(object);

      scene.add(chaves);
    });
  });
}

function onWindowResize() {
  camera.aspect = window.innerWidth / window.innerHeight;
  camera.updateProjectionMatrix();

  renderer.setSize(window.innerWidth, window.innerHeight);
}

/*
          move de maneira animada para uma posição determinada pelo seguinte alvo:
          var to = {
            x: posX,
            y: posY,
            z: posZ,
            rx: 0,
            ry: 0,
            rz: 0,
          };
        */
function move(to) {
  let duration = 100;

  let from = getCamCurrentPosition();
  tween = new TWEEN.Tween(from)
    .to(to, duration)
    .easing(TWEEN.Easing.Linear.None) // TWEEN.Easing.Quadratic.InOut ...
    .onUpdate(function () {
      camera.position.set(from.x, from.y, from.z);
      camera.rotation.set(from.rx, from.ry, from.rz);
      //controls.target.set(from.rx, from.ry, from.rz);
    })
    .start();
}

//Anima as chaves seccionadoras
function toggle3DSwitch() {
  let duration = 700;

  if (switchZero == null) {
    switchZero = chaves.children[0].rotation.z;
    switchPi = chaves.children[0].rotation.z + Math.PI / 2;
  }
  var angleTo2;
  var angleFrom2;
  if (!switchOpen) {
    angleFrom = { rot: chaves.children[0].rotation.z };
    angleTo = { rot: switchZero };
    angleFrom2 = { rot: chaves.children[3].rotation.z };
    angleTo2 = { rot: switchZero + Math.PI };
  } else {
    angleTo = { rot: switchPi };
    angleFrom = { rot: chaves.children[0].rotation.z };
    angleTo2 = { rot: switchPi };
    angleFrom2 = { rot: chaves.children[3].rotation.z };
  }
  tween2 = new TWEEN.Tween(angleFrom)
    .to(angleTo, duration)
    .easing(TWEEN.Easing.Quadratic.InOut) // TWEEN.Easing.Quadratic.InOut ...
    .onUpdate(function () {
      chaves.children[0].rotation.set(
        chaves.children[0].rotation.x,
        chaves.children[0].rotation.y,
        angleFrom.rot
      );
      chaves.children[1].rotation.set(
        chaves.children[0].rotation.x,
        chaves.children[0].rotation.y,
        angleFrom.rot
      );
      chaves.children[2].rotation.set(
        chaves.children[0].rotation.x,
        chaves.children[0].rotation.y,
        angleFrom.rot
      );
    })
    .start();
  tween3 = new TWEEN.Tween(angleFrom2)
    .to(angleTo2, duration)
    .easing(TWEEN.Easing.Quadratic.InOut) // TWEEN.Easing.Quadratic.InOut ...
    .onUpdate(function () {
      chaves.children[3].rotation.set(
        chaves.children[3].rotation.x,
        chaves.children[3].rotation.y,
        angleFrom2.rot
      );
      chaves.children[4].rotation.set(
        chaves.children[3].rotation.x,
        chaves.children[3].rotation.y,
        angleFrom2.rot
      );
      chaves.children[5].rotation.set(
        chaves.children[3].rotation.x,
        chaves.children[3].rotation.y,
        angleFrom2.rot
      );
    })
    .start();
}

function stop() {
  tween.stop();
}

function animate() {
  requestAnimationFrame(animate);
  TWEEN.update();
  //controls.update();
  scrollIt();
  renderer.render(scene, camera);
}

function getCamCurrentPosition() {
  return {
    x: camera.position.x,
    y: camera.position.y,
    z: camera.position.z,
    rx: camera.rotation.x,
    ry: camera.rotation.y,
    rz: camera.rotation.z,
  };
}

function render() {
  Nexus.beginFrame(renderer.getContext());
  renderer.render(scene, camera);
  Nexus.endFrame(renderer.getContext());
}

//calculata status da posição da página
function scrollStatus() {
  let doc = document.getElementById("content");
  let part = Math.floor(doc.scrollTop / doc.clientHeight);
  let tolerance = 30;
  let percentage =
    (100 * (doc.scrollTop - part * doc.clientHeight)) / doc.clientHeight;
  if (percentage < tolerance) {
    percentage = 0;
  } else {
    percentage = (100 * (percentage - tolerance)) / (100 - tolerance);
  }
  return {
    index: part,
    percent: percentage,
  };
}

function scrollIt() {
  let scrollInfos = scrollStatus();

  if (scrollInfos.index + 1 < positionsList.length) {
    var result = percentage2position(
      scrollInfos.percent,
      positionsList[scrollInfos.index],
      positionsList[scrollInfos.index + 1]
    );
    move(result);
  }
}

function percentage2position(percentage, from, to) {
  var final = {
    x: 0,
    y: 0,
    z: 0,
    rx: 0,
    ry: 0,
    rz: 0,
  };

  final.x = from.x + (percentage / 100) * (to.x - from.x);
  final.y = from.y + (percentage / 100) * (to.y - from.y);
  final.z = from.z + (percentage / 100) * (to.z - from.z);
  final.rx = from.rx + (percentage / 100) * (to.rx - from.rx);
  final.ry = from.ry + (percentage / 100) * (to.ry - from.ry);
  final.rz = from.rz + (percentage / 100) * (to.rz - from.rz);

  return final;
}
