window.cameraFOV = 45;
window.cameraNear = 0.005;
window.cameraFar = 100;
//defines camera pose for each section of the page
positionsList = [
  {
    x: 0.6475862646159432,
    y: 1.1997816125707526,
    z: 0.6190745660847056,
    rx: -1.1282887427513004,
    ry: 0.40805561683817815,
    rz: 0.6971650393993563,
  },
  {
    x: 0.6792061998260425,
    y: 0.007876494153314205,
    z: 0.3744274769755487,
    rx: -0.1421402088060801,
    ry: 1.0476831112595257,
    rz: 0.1233382759196245,
  },
  {
    x: 0.30073789358464986,
    y: -0.12398655775434014,
    z: 0.11853634648890665,
    rx: 0.3137915420376861,
    ry: 0.7437458993032806,
    rz: -0.21627507689235176,
  },
  {
    x: 0.23125676124593975,
    y: -0.1151109056740458,
    z: 0.07344040506408878,
    rx: 0.2132481824506399,
    ry: 0.08348900707158,
    rz: -0.018055798147838018,
  },
  {
    x: 0.23202606974920187,
    y: -0.1151109056740458,
    z: 0.061336870919358966,
    rx: 0.2132481824506399,
    ry: 0.08348900707158,
    rz: -0.018055798147838018,
  },
  {
    x: 0.41374480257353113,
    y: 0.1700366871570529,
    z: -0.5010730087502565,
    rx: -2.354235615675942,
    ry: 0.32043412021114165,
    rz: 2.8353268499384763,
  },
  {
    x: -0.32485144062379945,
    y: 0.08769578765466972,
    z: -0.5329030946410781,
    rx: -2.708583960807438,
    ry: -0.5979367563066578,
    rz: -2.8870108772216287 + 2 * Math.PI,
  },
  {
    x: -0.24504797443226237,
    y: 0.007505789649918211,
    z: -0.26123650187056197,
    rx: -1.674830331862977,
    ry: -0.6512578989979239,
    rz: -1.7413647691957466 + 2 * Math.PI,
  },
  {
    x: -1.0973432212809653,
    y: 0.034907920236185226,
    z: 0.007913745155948393,
    rx: -0.4533317569428489,
    ry: -1.2325940237464799,
    rz: -0.4307870505599675 + 2 * Math.PI,
  },
];

window.objectsToLoad = [
  {
    name: "se",
    path: "./models/se.nxs",
    onLoadAnimation: {
      objName: "se",
      duration: 2000,
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
          initialState: 0,
        },
        {
          property: "position",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
          initialState: [0, -500, 0],
        },
      ],
    },
  },
  {
    name: "ortophoto",
    path: "./models/solo.fbx",
  },
  {
    name: "chaves",
    children: [
      {
        name: "sec1",
        path: "./models/sec1.fbx",
        position: [
          { x: -4378.6, y: 303, z: -1271 },
          { x: -4378.6, y: 303, z: -965 },
          { x: -4378.6, y: 303, z: -690 },
        ],
      },
      {
        name: "sec2",
        path: "./models/sec2.fbx",
        position: [
          { x: -4178.1, y: 303, z: -1271 },
          { x: -4178.1, y: 303, z: -965 },
          { x: -4178.1, y: 303, z: -690 },
        ],
      },
    ],
  },
];

window.animationKeys = {
  sec1: {
    open: {
      objName: "sec1",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, Math.PI / 2, 0],
          target: "children",
        },
      ],
    },
    close: {
      objName: "sec1",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "children",
        },
      ],
    },
  },
  sec2: {
    open: {
      objName: "sec2",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, -Math.PI / 2, 0],
          target: "children",
        },
      ],
    },
    close: {
      objName: "sec2",
      poseKeys: [
        {
          property: "rotation",
          type: "absolute",
          value: [0, 0, 0],
          target: "children",
        },
      ],
    },
  },
  se: {
    reduceOpacity: {
      objName: "se",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 0.5,
          target: "children",
        },
      ],
    },
    increaseOpacity: {
      objName: "se",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
        },
      ],
    },
    showUp: {
      objName: "se",
      poseKeys: [
        {
          property: "Opacity",
          type: "absolute",
          value: 1,
          target: "children",
          initialState: 0,
        },
        {
          property: "position",
          type: "absolute",
          value: [0, 0, 0],
          target: "obj",
          initialState: [0, -400, 0],
        },
      ],
    },
  },
};
